package io.finer.ads.jeecg.service;

import io.finer.ads.jeecg.entity.ClickLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 点击日志
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
public interface IClickLogService extends IService<ClickLog> {

}
